// clicker - detect object clicks
// Send link message based on clicks on a prom
//
// Detect clicks on the sides of the prim

// Display float text
integer SHOW_FLOAT = TRUE;

send(string message) {
    llMessageLinked(LINK_ROOT, 0, message, llDetectedKey(0));
}

default {
    touch_start(integer total_number) {
        integer link    = llDetectedLinkNumber(0);
        integer face    = llDetectedTouchFace(0);
        vector p = llDetectedTouchST(0);
        if (face == 0 && p.x > 0.9) {
            send("reset");
        } else {
            send("display");
        }        
    }

    on_rez(integer start_param) {
        llResetScript();
    }
}
