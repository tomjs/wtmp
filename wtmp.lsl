// Online Logger - Similar to UNIX wtmp information
// Monitors a notecard list of UUIDs and records online state changes
//
// v1   - Count agents online
// v2   - Support HTTP POST logging, VR display
// v3   - Searches for agents in region rather than online
// v3.1 - Catch agents leaving in region mode
// v4   - Whitelist multiple users
// v5   - Do all user auth in this script
// v6   - Eliminate the button scripts (use button names)
// v7   - Add option to skip display names
// v8   - Listen for remote commands
// v9   - Add Discord report

//////////////////////////////////////////////////////////////////////
// Last online scanner -- see when given avatars were last online
// By Antony Fairport.
//
// Revision history.
// =================
//
// 2011-05-07
// Initial version.

//////////////////////////////////////////////////////////////////////
// Constants.
float  REFRESH_INTERVAL = 30.0;

integer CHANNEL = 80085;

string KEY_FILE         = "Keys";
key report = "6c8ff91f-d74b-4842-b38b-81bb08f6efde";
list AUTHORIZED_USERS = [
    "6c8ff91f-d74b-4842-b38b-81bb08f6efde"      // tomjs
];

string WEBHOOK_CHANNEL = "123456789";
string WEBHOOK_TOKEN =  "abcdefghijklmnopqrstuvwxyz";
string WEBHOOK_URL = "https://discordapp.com/api/webhooks/";
integer WEBHOOK_WAIT = TRUE;

string wtmp_url = "http://example.com/log/";

// Slow down name lookups
float pokey = 2.0;

// Skip display name lookups
integer LOOKUP_DISPLAY_NAMES = TRUE;

integer report_Hover = FALSE;
integer report_IM = TRUE;
integer report_HTTP = FALSE;
integer report_DISCORD = TRUE;

// Does current region only if TRUE, grid-wide (online status only) if FALSE
integer REGION_MODE = FALSE;
string REGION_NAME = "<set me>";

integer MEM_LIMIT = 40000;

//////////////////////////////////////////////////////////////////////
// Globals.
integer g_iMaxTracking;
integer g_iCurrent;
list    g_lTracking;
list    g_lUserNames;
list    g_lDisplayNames;
list    g_lLastSeen;
list    g_lLastStatus;
list    g_lRegion;
key     g_keyRequest;
key     g_keyKey;
integer g_iKey;
integer g_iOnline;
key     http_request_id;

//////////////////////////////////////////////////////////////////////
// Return a string that is a formatted log line
string FormatLog() {
    string sUN   = llList2String( g_lUserNames, g_iCurrent );
    string sDN   = llList2String( g_lDisplayNames, g_iCurrent );
    string sName = sDN;

    if ( sUN != sDN ) {
        sName = sDN + " (" + sUN + ")";
    }

    return sName;
}

//////////////////////////////////////////////////////////////////////
// Return a string that is the report of last online times.
string LastOnlineReport() {
    string s = "";
    integer i;

    for ( i = 0; i < g_iMaxTracking; i++ ) {
        string sUN   = llList2String( g_lUserNames, i );
        string sDN;
        string sName;

        if (LOOKUP_DISPLAY_NAMES) {
            sDN   = llList2String( g_lDisplayNames, i );
            sName = sDN;
            if ( sUN != sDN ) {
                sName = sDN + " (" + sUN + ")";
            }
        } else {
            sName = sUN;
        }

        s += sName + " : " + llList2String(g_lLastSeen, i) + " : " + llList2String(g_lLastStatus, i) + "\n";
    }

    return s;
}

UpdateDisplay(integer num) {
    // Set color
    llMessageLinked(LINK_ROOT, 204006, "<0,1,0>", "0");

    // Send message
    llMessageLinked(LINK_ROOT, 204000, " " + (string)num + " of " + (string)llGetListLength(g_lLastStatus), "0");
}

//////////////////////////////////////////////////////////////////////
// Format a llGetTimestamp()
string FormatTime( string sTime ) {
    list l = llParseString2List( sTime, [ "T", "." ], [] );

    return llList2String( l, 0 ) + " " + llList2String( l, 1 ) + " UTC";
}


//////////////////////////////////////////////////////////////////////
// Post wtmp to Discord

key post_discord(string message) {
    list body = [
        "username", llGetObjectName(),
        "content", message,
        "tts", "false"
    ];

    string query_string = "";
    if (WEBHOOK_WAIT)
        query_string += "?wait=true";

    return llHTTPRequest(
        WEBHOOK_URL + WEBHOOK_CHANNEL + "/" + WEBHOOK_TOKEN + query_string,
        [
            HTTP_METHOD, "POST",
            HTTP_MIMETYPE, "application/json",
            HTTP_VERIFY_CERT,TRUE,
            HTTP_VERBOSE_THROTTLE, TRUE,
            HTTP_PRAGMA_NO_CACHE, TRUE
        ],
        llList2Json(JSON_OBJECT, body)
    );
}

//////////////////////////////////////////////////////////////////////
// Write to the remote wtmp logger
log_wtmp(string agent, string msg) {
    if (report_IM && agent != NULL_KEY) {
        llInstantMessage(agent, msg);
    }
    if (report_HTTP) {
        string body;

        body = "msg=" + llEscapeURL(msg);

        http_request_id = llHTTPRequest(
            wtmp_url,
            [
                HTTP_METHOD, "POST",
                HTTP_MIMETYPE,"application/x-www-form-urlencoded"
            ],
            body
        );
    }
    if (report_DISCORD) {
        post_discord("\n" + msg);
    }
}

display(key id) {
    if (
        id == llGetOwnerKey(llGetKey()) ||
        llListFindList(AUTHORIZED_USERS, [(string)id]) >= 0
    ) {
        llInstantMessage(id, "\n" + LastOnlineReport());
        if (report_DISCORD) {
            post_discord("\n" + LastOnlineReport());
        }
    }
}

reset() {
    llResetScript();
}

// Return TRUE if:
// * owner
// * key in AUTHORIZED_USERS
// * name(key) in AUTHORIZED_USERS
// * group matches object group
integer authorized(key id) {
//    log("key->username: " + llGetUsername(id));
    return (
        id == llGetOwnerKey(llGetKey()) ||
        llListFindList(AUTHORIZED_USERS, [(string)id]) >= 0 ||
        llListFindList(AUTHORIZED_USERS, [llGetUsername(id)]) >= 0 ||
        llSameGroup(id)
    );
}

command(string msg, key id) {
//    log("command("+msg+", "+(string)id+")");
    string cmd = "";
    integer idx = llSubStringIndex(msg, " ");
    if (idx == -1) {
        cmd = msg;
    } else {
        cmd = llGetSubString(msg, 0, idx-1);
        msg = llDeleteSubString(msg, 0, idx);
    }
    cmd = llToLower(cmd);
//    log("  cmd="+cmd);

    if (authorized(id)) {
        if (cmd == "reset" || cmd == "r") {
            reset();
        }
        else if (cmd == "debug") {
//            VERBOSE = !VERBOSE;
//            log("Debug toggled");
        }
        else if (cmd == "display" || cmd == "d") {
            display(id);
        }
    }
}


//////////////////////////////////////////////////////////////////////
// Default state.
default {
    //////////////////////////////////////////////////////////////////
    // State entry.
    state_entry() {
        // Set up memory constraints
        llSetMemoryLimit(MEM_LIMIT);
//        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
        log_wtmp(report, "wtmp startup");

        // Simply jump to the setup state.
        state Setup;
    }
}

//////////////////////////////////////////////////////////////////////
// Setup state.
state Setup {
    //////////////////////////////////////////////////////////////////
    // State entry.
    state_entry() {
        // Let the user know what's happening.
        llSetText( "Reading list of keys to track, please wait...", < 1.0, 0.0, 0.0 >, 1.0 );
        UpdateDisplay(0);

        // Clear out the lists and get the tracking list length
        g_lTracking     = [];
        g_lUserNames    = [];
        g_lDisplayNames = [];
        g_lLastSeen     = [];
        g_lLastStatus   = [];

        // If we have the keys file...
        if ( llGetInventoryType( KEY_FILE ) == INVENTORY_NOTECARD ) {
            // Start reading.
            g_keyKey = llGetNotecardLine( KEY_FILE, g_iKey = 0 );
        }
    }

    //////////////////////////////////////////////////////////////////
    // Handle data server responses.
    dataserver( key queryid, string data ) {
        // If this is our query...
        if ( queryid == g_keyKey ) {
            // Record how many we're tracking
            g_iMaxTracking  = llGetListLength(g_lTracking);

            // If this isn't the end of the file...
            if ( data != EOF ) {

                integer ix = llSubStringIndex(data,"//");                       //remove comments
                if (ix != -1) {
                    if (ix == 0) data = "";
                    else data = llGetSubString(data, 0, ix - 1);
                }

                // Check for assignment
                integer i = llSubStringIndex(data, "=");

                // If the data looks like a key...
                if ((i == -1) && (data != "") && (data != "#") && (((key)data ) != NULL_KEY) ) {
                    // Extend the lists.
                    g_lTracking     += [ data ];
                    g_lUserNames    += [ "" ];
                    g_lDisplayNames += [ "" ];
                    g_lLastSeen     += [ "No login" ];
                    g_lLastStatus   += [ "off" ];
                } else {
                    if (i != -1) {
                        string attr = llToLower(llStringTrim(llGetSubString(data, 0, i-1), STRING_TRIM));
                        string value = llStringTrim(llGetSubString(data, i+1, -1), STRING_TRIM);

                        if (attr == "last_seen") {
                            // Update last seen value for current avi
                            g_lLastSeen = llListReplaceList(
                                g_lLastSeen,
                                [value],
                                g_iMaxTracking-1,
                                g_iMaxTracking-1
                            );
                        }
                    }
                }

                // Next key...
                g_keyKey = llGetNotecardLine( KEY_FILE, ++g_iKey );
            } else {
                // Start by getting user names...
                state SetupUserNames;
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////
// User name setup state.
state SetupUserNames {
    //////////////////////////////////////////////////////////////////
    // State entry.
    state_entry() {
        // Let the user know what's happening.
        llSetText( "Getting user names, plase wait...", < 1.0, 0.0, 0.0 >, 1.0 );

        // Starting with the first entry in the tracking list...
        g_iCurrent = 0;

        // Start getting user names...
        g_keyRequest = llRequestAgentData( llList2Key( g_lTracking, g_iCurrent ), DATA_NAME );
    }

    //////////////////////////////////////////////////////////////////
    // Handle data server responses.
    dataserver( key queryid, string data ) {
        // If this is our query...
        if ( queryid == g_keyRequest ) {
            // Put the name into the list.
            g_lUserNames = llListReplaceList( g_lUserNames, [ data ], g_iCurrent, g_iCurrent );

            // Move on.
            g_iCurrent++;

            // If we've got some names left to get...
            if ( g_iCurrent < g_iMaxTracking ) {
                // Get the next name...
                g_keyRequest = llRequestAgentData( llList2Key( g_lTracking, g_iCurrent ), DATA_NAME );
            } else {
                if (LOOKUP_DISPLAY_NAMES) {
                    // Now we get display names.
                    state SetupDisplayNames;
                } else {
                    // Now we can start online scanning.
                    state OnlineScanner;
                }
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////
// Display name setup state.
state SetupDisplayNames {
    //////////////////////////////////////////////////////////////////
    // State entry.
    state_entry() {
        // Let the user know what's happening.
        llSetText( "Getting display names, please wait...", < 1.0, 0.0, 0.0 >, 1.0 );

        // Starting with the first entry in the tracking list...
        g_iCurrent = 0;

        // Start getting display names...
        g_keyRequest = llRequestDisplayName( llList2Key( g_lTracking, g_iCurrent ) );
    }

    //////////////////////////////////////////////////////////////////
    // Handle data server responses.
    dataserver( key queryid, string data ) {
        // If this is our query...
        if ( queryid == g_keyRequest ) {
            // Put the name into the list.
            g_lDisplayNames = llListReplaceList( g_lDisplayNames, [ data ], g_iCurrent, g_iCurrent );

            // Move on.
            g_iCurrent++;

            // If we've got some names left to get...
            if ( g_iCurrent < g_iMaxTracking ) {
                // Throttle
                llSleep(pokey);
                // Get the next name...
                g_keyRequest = llRequestDisplayName( llList2Key( g_lTracking, g_iCurrent ) );
            } else {
                llSetText( "Starting online scanner, please wait...", < 0.0, 1.0, 0.0 >, 1.0 );

                // Now we can start online scanning.
                state OnlineScanner;
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////
// The online scanner state.
state OnlineScanner {
    //////////////////////////////////////////////////////////////////
    // State entry.
    state_entry() {
        // Get all agents in the region
        g_lRegion = llGetAgentList(AGENT_LIST_REGION, []);

        if (llGetListLength(g_lRegion) > 0 || ! REGION_MODE) {
            // Starting with the first entry in the tracking list
            g_iCurrent = 0;

            // Start getting the online status.
            g_keyRequest = llRequestAgentData(llList2Key(g_lTracking, g_iCurrent), DATA_ONLINE);
        } else {
            // Pause for a while.
            state ScannerPause;
        }
    }

    //////////////////////////////////////////////////////////////////
    // Handle data server responses.
    dataserver( key queryid, string data ) {
        // If this is our query...
        if ( queryid == g_keyRequest ) {
            // Check last status
            string laststatus = llList2String(g_lLastStatus, g_iCurrent);
            string timestamp = FormatTime(llGetTimestamp());

            if (REGION_MODE) {
                integer i = 0;
                integer found = FALSE;
                do {
                    key agent = llList2Key(g_lRegion, i);
                    if ((string)agent == llList2String(g_lTracking, g_iCurrent)) {
                        // If the avatar is in the region...
                        found = TRUE;
                        if ( data == "1" ) {
                            // Update their last seen value.
                            g_lLastSeen = llListReplaceList( g_lLastSeen, [ timestamp ], g_iCurrent, g_iCurrent );
                            if (laststatus == "off") {
                                g_lLastStatus = llListReplaceList( g_lLastStatus, [ "on" ], g_iCurrent, g_iCurrent );
                                log_wtmp(report, timestamp + " " + FormatLog() + " entered " + REGION_NAME);
                                g_iOnline++;
                            }
                        } else {
                            if (laststatus == "on") {
                                g_lLastStatus = llListReplaceList( g_lLastStatus, [ "off" ], g_iCurrent, g_iCurrent );
                                log_wtmp(report, timestamp + " " + FormatLog() + " left " + REGION_NAME);
                                g_iOnline--;
                            }
                        }
                    }
                    i++;
                } while(i <= llGetListLength(g_lRegion));

                // Catch monitored agents not in region
                if (!found && laststatus == "on") {
                    g_lLastStatus = llListReplaceList( g_lLastStatus, [ "off" ], g_iCurrent, g_iCurrent );
                    log_wtmp(report, timestamp + " " + FormatLog() + " left " + REGION_NAME);
                    g_iOnline--;
                }
            } else {
                // If the avatar is online...
                if ( data == "1" ) {
                    // Update their last seen value.
                    g_lLastSeen = llListReplaceList( g_lLastSeen, [ timestamp ], g_iCurrent, g_iCurrent );
                    if (laststatus == "off") {
                        g_lLastStatus = llListReplaceList( g_lLastStatus, [ "on" ], g_iCurrent, g_iCurrent );
                        log_wtmp(report, timestamp + " " + FormatLog() + " logon");
                        g_iOnline++;
                    }
                } else {
                    if (laststatus == "on") {
                        g_lLastStatus = llListReplaceList( g_lLastStatus, [ "off" ], g_iCurrent, g_iCurrent );
                        log_wtmp(report, timestamp + " " + FormatLog() + " logoff");
                        g_iOnline--;
                    }
                }
            }

            // Move on.
            g_iCurrent++;
            UpdateDisplay(g_iOnline);

            // If we've got some avatars left to check...
            if ( g_iCurrent < g_iMaxTracking ) {
                // Get the next status...
                g_keyRequest = llRequestAgentData( llList2Key( g_lTracking, g_iCurrent ), DATA_ONLINE );
            } else {
                if (report_Hover) {
                    // Update the display.
                    llSetText(LastOnlineReport(), <1.0, 1.0, 1.0>, 1.0);
                } else {
                    llSetText("", <1.0, 1.0, 1.0>, 1.0);
                }

                // Pause for a while.
                state ScannerPause;
            }
        }
    }

    //////////////////////////////////////////////////////////////////
    // Look for changes.
    changed( integer change ) {
        // Did the inventory of the object change?
        if ( change & CHANGED_INVENTORY ) {
            // Yes. Most likely the key list was changed. Start over.
            llResetScript();
        }
    }
}

//////////////////////////////////////////////////////////////////////
// Scan pause state.
state ScannerPause {
    //////////////////////////////////////////////////////////////////
    // State entry.
    state_entry() {
//        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
        // Set up the timer for the pause.
        llSetTimerEvent( REFRESH_INTERVAL );

        llListen(CHANNEL, "", NULL_KEY, "");
    }

    touch_start(integer i) {
        // Use the button name as the command
        string msg = llGetLinkName(llDetectedLinkNumber(0));
        command(msg, llDetectedKey(0));
    }

    //////////////////////////////////////////////////////////////////
    // Timer event.
    timer() {
        // Kill the timer.
        llSetTimerEvent( 0.0 );

        // Go back and scan again.
        state OnlineScanner;
    }

    //////////////////////////////////////////////////////////////////
    // Look for changes.
    changed( integer change ) {
        // Did the inventory of the object change?
        if ( change & CHANGED_INVENTORY ) {
            // Yes. Most likely the key list was changed. Start over.
            llResetScript();
        }
    }

    listen(integer channel, string name, key id, string msg) {
        command(msg, id);
    }

    link_message(integer sender_num, integer num, string msg, key id) {
        command(msg, id);
    }

}
