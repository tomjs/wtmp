wtmp
====

Avatar logger

LICENSE
=======

The scripts included here are licensed under the GNU Public License v3.
The script source code may be found in my GitLab repository at
https://gitlab.com/tomjs/wtmp/tree/master.

Enjoy!
Layne Thomas (tomjs)
